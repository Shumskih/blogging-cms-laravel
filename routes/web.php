<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/summernote', 'SummerController@index')->name('summernote');

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

    // Home
    Route::get('/home', 'HomeController@index')->name('home');

    // Posts
    Route::get('/post/create', 'PostController@create')->name('post.create');

    Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');

    Route::post('/post/update/{id}', 'PostController@update')->name('post.update');

    Route::get('/post/delete/{id}', 'PostController@destroy')->name('post.delete');

    Route::post('/post/store', 'PostController@store')->name('post.store');

    Route::get('/posts', 'PostController@index')->name('posts');

    Route::get('/posts/trashed', 'PostController@trashed')->name('posts.trashed');

    Route::get('/posts/kill/{id}', 'PostController@kill')->name('post.kill');

    Route::get('/posts/restore/{id}', 'PostController@restore')->name('post.restore');

    // Categories
    Route::get('/category/create', 'CategoryController@create')->name('category.create');

    Route::post('/category/store', 'CategoryController@store')->name('category.store');

    Route::get('/categories', 'CategoryController@index')->name('categories');

    Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');

    Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');

    Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('category.delete');

    // Tags
    Route::get('/tag/create', 'TagController@create')->name('tag.create');

    Route::post('/tag/store', 'TagController@store')->name('tag.store');

    Route::get('/tags', 'TagController@index')->name('tags');

    Route::get('/tag/edit/{id}', 'TagController@edit')->name('tag.edit');

    Route::post('/tag/update/{id}', 'TagController@update')->name('tag.update');

    Route::get('/tag/delete/{id}', 'TagController@destroy')->name('tag.delete');

    // Users
    Route::get('/users', 'UserController@index')->name('users');

    Route::get('/user/create', 'UserController@create')->name('user.create');

    Route::post('/user/store', 'UserController@store')->name('user.store');

    Route::get('/user/admin/{id}', 'UserController@admin')->name('user.admin');

    Route::get('/user/not-admin/{id}', 'UserController@notAdmin')->name('user.not.admin');

    Route::get('/user/profile', 'ProfileController@index')->name('user.profile');

    Route::get('/user/delete/{id}', 'UserController@destroy')->name('user.delete');

    Route::post('/user/profile/update', 'ProfileController@update')->name('user.profile.update');
});